import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { AppComponent } from './app.component';
import { DakaComponent } from './daka/daka.component';
import { QingjiaComponent } from './qingjia/qingjia.component';
import { WaichuComponent } from './waichu/waichu.component';
import { AuthGuardService } from './service/auth-guard.service';

import { ProfileComponent } from './profile/profile.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { NavbarComponent } from './navbar/navbar.component';
import {ShenpiComponent} from './shenpi/shenpi.component';

const routes: Routes = [

  {path: 'qingjia', component: QingjiaComponent, canActivate: [AuthGuardService]},
  {path: 'waichu', component: WaichuComponent, canActivate: [AuthGuardService]},
  {path: 'daka', component: DakaComponent, canActivate: [AuthGuardService]},
  {path: 'shenpi', component: ShenpiComponent, canActivate: [AuthGuardService]},
  {path: 'login', component: LoginComponent},
  {path: 'profile', component: ProfileComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
