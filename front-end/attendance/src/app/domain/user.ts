export class User {
  sid: number;
  sname: string;
  job: string;
  pass: string;
  level: number;
}
