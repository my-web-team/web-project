export class Waichu {
  oid: number;
  sid: number;
  startTime: string;
  endTime: string;
  reason: string;
  state: string;
}
