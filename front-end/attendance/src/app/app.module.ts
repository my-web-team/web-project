import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { DakaComponent } from './daka/daka.component';
import { QingjiaComponent } from './qingjia/qingjia.component';
import { WaichuComponent } from './waichu/waichu.component';
import { ProfileComponent } from './profile/profile.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { NavbarComponent } from './navbar/navbar.component';
import { ShenpiComponent } from './shenpi/shenpi.component';

import { ApiService } from './service/api.service';
import { QingjiaService } from './service/qingjia.service';
import { WaichuService } from './service/waichu.service';
import { DakaService } from './service/daka.service';
import { ShenpiService } from './service/shenpi.service';
import { AuthService } from './service/auth.service';
import { AuthGuardService } from './service/auth-guard.service';
import { UserService } from './service/user.service';

@NgModule({
  declarations: [
    AppComponent,
    DakaComponent,
    QingjiaComponent,
    WaichuComponent,
    ShenpiComponent,
    ProfileComponent,
    LoginComponent,
    RegisterComponent,
    NavbarComponent
  ],
  imports: [
    FormsModule,
    HttpModule,
    BrowserModule,
    AppRoutingModule,
  ],
  providers: [
    ApiService,
    QingjiaService,
    WaichuService,
    DakaService,
    ShenpiService,
    AuthService,
    AuthGuardService,
    UserService
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
