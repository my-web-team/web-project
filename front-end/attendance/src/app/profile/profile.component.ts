import { Component, OnInit } from '@angular/core';
import { User } from '../domain/user';
import { UserService } from '../service/user.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  constructor(private userService: UserService) { }

  users: User [];
  ngOnInit() {
    this.userService.selectUser(localStorage.getItem('userId'), '')
      .then(users => this.users = users);
  }
}
