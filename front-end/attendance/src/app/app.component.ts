import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { QingjiaComponent } from './qingjia/qingjia.component';
import { DakaComponent } from './daka/daka.component';
import { WaichuComponent } from './waichu/waichu.component';
import { ProfileComponent } from './profile/profile.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { UserComponent } from './user/user.component';
import { UserService } from './service/user.service';
import { User } from './domain/user';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {

  constructor(private userService: UserService, private router: Router) {}

  title = 'attendance';
  user: User [];

  ngOnInit() {
    if(localStorage.getItem('userId')!=null) {
      var tmp = document.getElementById('denglu');
      tmp.style.display = 'none';
      tmp=document.getElementById('zhuce');
      tmp.style.display = 'none';
      tmp=document.getElementById('zhongxin');
      tmp.style.display = 'inline';
      tmp=document.getElementById('zhuxiao');
      tmp.style.display = 'inline';
    }
  }

  check() { }
  logout_click() {
    localStorage.removeItem('userId');
    localStorage.removeItem('level');
    var tmp = document.getElementById('denglu');
    tmp.style.display = 'inline';
     tmp=document.getElementById('zhuce');
     tmp.style.display='inline';

     tmp=document.getElementById('zhongxin');
     tmp.style.display='none';
     tmp=document.getElementById('zhuxiao');
     tmp.style.display='none';
  }

  submit_click() {
    if (this.check() === true) {
     var tmp = document.getElementById('dl');
     tmp.style.display = 'none';
     tmp = document.getElementById('denglu');
     tmp.style.display = 'none';
     tmp=document.getElementById('zhuce');
     tmp.style.display = 'none';

     tmp=document.getElementById('zhongxin');
     tmp.style.display = 'inline';
     tmp=document.getElementById('zhuxiao');
     tmp.style.display = 'inline';
    } else {
      alert("用户名或密码错误!");
    }
  }
  cancel_click() {
     var tmp = document.getElementById('dl');
     tmp.style.display = 'none';
  }
  daka_click() {
    document.getElementById("b1").style.display="block";
    document.getElementById("b2").style.display="block";
    document.getElementById("a1").style.display="none";
    document.getElementById("a2").style.display="none";
    document.getElementById("a3").style.display="none";
    document.getElementById("c1").style.display="none";
    document.getElementById("c2").style.display="none";
    document.getElementById("c3").style.display="none";

  }
  qingjia_click() {
    document.getElementById("b1").style.display="none";
    document.getElementById("b2").style.display="none";
    document.getElementById("a1").style.display="none";
    document.getElementById("a2").style.display="none";
    document.getElementById("a3").style.display="none";
    document.getElementById("c1").style.display="block";
    document.getElementById("c2").style.display="block";
    document.getElementById("c3").style.display="block";
  }
  waichu_click() {
    document.getElementById("b1").style.display="none";
    document.getElementById("b2").style.display="none";
    document.getElementById("a1").style.display="block";
    document.getElementById("a2").style.display="block";
    document.getElementById("a3").style.display="block";
    document.getElementById("c1").style.display="none";
    document.getElementById("c2").style.display="none";
    document.getElementById("c3").style.display="none";
  }
  shenpi_click() {
    console.log(localStorage.getItem('level'));
    if (localStorage.getItem('level') === '1'
      || localStorage.getItem('level') === null) {
      alert('抱歉，您没有权限查看！');
      this.router.navigate(['/daka']);
    } else {
      this.router.navigate(['/shenpi']);
    }
    document.getElementById("b1").style.display="none";
    document.getElementById("b2").style.display="none";
    document.getElementById("a1").style.display="none";
    document.getElementById("a2").style.display="none";
    document.getElementById("a3").style.display="none";
    document.getElementById("c1").style.display="none";
    document.getElementById("c2").style.display="none";
    document.getElementById("c3").style.display="none";
  }
}
