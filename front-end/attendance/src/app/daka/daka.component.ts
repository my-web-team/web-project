import { Component, OnInit } from '@angular/core';
import { Daka } from '../domain/daka';
import { DakaService } from '../service/daka.service';

@Component({
  selector: 'app-daka',
  templateUrl: './daka.component.html',
  styleUrls: ['./daka.component.css']
})
export class DakaComponent implements OnInit {
  constructor(private dakaService: DakaService) { }
  dakas: Daka [];
  ngOnInit() {
    this.dakaService.selectDaka('',
      localStorage.getItem('userId'), '')
      .then(dakas => this.dakas = dakas);
  }

  addDaka() {
    console.log(new Date().toLocaleDateString());
    this.dakaService.createConggeDaka(localStorage.getItem('userId'), new Date().toLocaleDateString());
    console.log(this.dakas);
  }
}
