import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WaichuComponent } from './waichu.component';

describe('WaichuComponent', () => {
  let component: WaichuComponent;
  let fixture: ComponentFixture<WaichuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WaichuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WaichuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
