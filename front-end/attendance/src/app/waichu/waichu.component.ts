import { Component, OnInit } from '@angular/core';
import { Waichu } from '../domain/waichu';
import { WaichuService } from '../service/waichu.service';
import { Waichu } from '../domain/waichu';

@Component({
  selector: 'app-waichu',
  templateUrl: './waichu.component.html',
  styleUrls: ['./waichu.component.css']
})
export class WaichuComponent implements OnInit {

  constructor(private waichuService: WaichuService) { }
  waichus: Waichu [];
  allWaichu: Waichu [];
  selectedWaichu: Waichu [];
  rId: number;
  
  ngOnInit() {
    this.waichuService.selectWaichu(0, localStorage.getItem('userId'), '', '', '', '')
      .then(waichus => this.waichus = waichus);
    this.waichuService.getWaichu()
      .then(waichus => this.allWaichu = waichus);
  }

  addWaichu(st: string, et: string, reason: string) {
    st = st.trim();
    et = et.trim();
    reason = reason.trim();
    var r = confirm('确认提交吗？');
    if (r == true) {
      if (!st || !et || !reason) {
        alert('请填写完全你的理由！');
        return false;
      } else {
        this.waichuService.createConggeWaichu(localStorage.getItem('userId'), st, et, reason);
        alert('你已经提交了请求！');
        return true;
      }
    } else {
      alert('已取消操作');
      return false;
    }
  }
  deleteWaichu(id: number) {
    this.waichuService.deleteWaichu(id);
  }

  updatewaichu(id: number) {
    this.rId = id;
    this.waichuService.selectWaichu(id, '', '', '', '', '')
      .then(waichus => this.selectedWaichu = waichus);
    if ((this.selectedWaichu[0] as waichu).state === '待定') {
      document.getElementById('renew').style.display = 'block';
      (document.getElementById('r-st') as HTMLInputElement).value = (this.selectedWaichu[0] as waichu).startTime;
      (document.getElementById('r-et') as HTMLInputElement).value = (this.selectedWaichu[0] as waichu).endTime;
      (document.getElementById('r-reason') as HTMLInputElement).value = (this.selectedWaichu[0] as waichu).reason;
    } else {
      alert('本状态无法更改！');
    }
  }

  rAddwaichu(type: string, st: string, et: string, reason: string) {
    let waichu = {
      lid: this.rId,
      sid: localStorage.getItem('userId'),
      startTiime: st,
      endTime: et,
      reason: reason,
      state: '待定'
    };
    this.waichuService.updateWaichu(this.rId, JSON.stringify(waichu));
    document.getElementById('renew').style.display = 'none';
  }
}
