import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/operators';

import { Waichu } from '../domain/waichu';

import { ApiService } from './api.service';
import {Daka} from '../domain/daka';

@Injectable()
export class WaichuService {
  private api_url ;
  private headers ;

  constructor(private http: Http, private apiService: ApiService) {
    this.api_url = apiService.getUrl() + '/outs';
    this.headers = apiService.getHeaders();
  }
  getWaichu(): Promise<Waichu[]> {
    const url = `${this.api_url}/all`;
    return this.http.get(url, {headers: this.headers})
      .toPromise()
      .then(res => res.json() as Waichu[])
      .catch(this.handleError);
  }

  getWaichuById(id: number): Promise<Waichu[]> {
    const url = `${this.api_url}/one/${id}`;
    return this.http.get(url, {headers: this.headers})
      .toPromise()
      .then(res => res.json() as Waichu[])
      .catch(this.handleError);
  }

  selectWaichu(oid: number, sid: string, startTime: string,
             endTime: string, reason: string, state: string): Promise<Waichu[]> {
    const url = `${this.api_url}/select`;
    let waichu = {
      oid: oid,
      sid: sid,
      startTime: startTime,
      endTime: endTime,
      reason: reason,
      state: state,
    };
    return this.http.post(url, JSON.stringify(waichu), {headers: this.headers})
      .toPromise()
      .then(res => res.json() as Waichu[])
      .catch(this.handleError);
  }

  createWaichu(waichu: Waichu): Promise<Waichu> {
    const url = `${this.api_url}`;
    return this.http
      .post(url, JSON.stringify(waichu), {headers: this.headers})
      .toPromise()
      .then(res => res.json() as Waichu)
      .catch(this.handleError);
  }

  createConggeWaichu(sid: string, st: string, et: string, reason: string): Promise<Waichu> {
    const url = `${this.api_url}/insert`;
    let waichu = {
      oid: 0,
      sid: sid,
      startTime: st,
      endTime: et,
      reason: reason,
      state: '待定'
    };
    let s = JSON.stringify(waichu);
    return this.http
      .post(url, s, {headers: this.headers})
      .toPromise()
      .then(res => res.json() as Waichu)
      .catch(this.handleError);
  }

  updateWaichu(id: number, waichu: string): Promise<Waichu> {
    const url = `${this.api_url}/update/${id}`;
    return this.http
      .post(url, waichu, { headers: this.headers })
      .toPromise()
      .then(res => res.json() as Waichu)
      .catch(this.handleError);
  }

  deleteWaichu(id: number): Promise<void> {
    const url = `${this.api_url}/delete`;
    let waichu = {
      oid: id,
      sid: '',
      startTime: '',
      endTime: '',
      reason: '',
      state: ''
    };
    return this.http
      .post(url, JSON.stringify(waichu) , { headers: this.headers })
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }

}
