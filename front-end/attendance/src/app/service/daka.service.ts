import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/operators';

import { Daka } from '../domain/daka';

import { ApiService } from './api.service';

@Injectable()
export class DakaService {
  private api_url ;
  private headers ;

  constructor(private http: Http, private apiService: ApiService) {
    this.api_url = apiService.getUrl() + '/clocks';
    this.headers = apiService.getHeaders();
  }
  getDaka(): Promise<Daka[]> {
    const url = `${this.api_url}/all`;
    return this.http.get(url, {headers: this.headers})
      .toPromise()
      .then(res => res.json() as Daka[])
      .catch(this.handleError);
  }

  getDakaById(id: string): Promise<Daka[]> {
    const url = `${this.api_url}/one/${id}`;
    return this.http.get(url, {headers: this.headers})
      .toPromise()
      .then(res => res.json() as Daka[])
      .catch(this.handleError);
  }

  selectDaka(lid: string, sid: string, cTime: string): Promise<Daka[]> {
    const url = `${this.api_url}/select`;
    let daka = {
      cid: lid,
      sid: sid,
      cTime: cTime,
    };
    return this.http.post(url, JSON.stringify(daka), {headers: this.headers})
      .toPromise()
      .then(res => res.json() as Daka[])
      .catch(this.handleError);
  }

  createConggeDaka(sid: string, cTime: string): Promise<Daka> {
    const url = `${this.api_url}/insert`;
    let daka = {
      cid: 0,
      sid: sid,
      cTime: cTime
    };
    let s = JSON.stringify(daka);
    return this.http
      .post(url, s, {headers: this.headers})
      .toPromise()
      .then(res => res.json() as Daka)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }

}
