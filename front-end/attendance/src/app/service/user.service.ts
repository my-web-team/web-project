import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/operators';

import { User } from '../domain/User';

import { ApiService } from './api.service';

@Injectable()
export class UserService {
  private api_url ;
  private headers ;

  constructor(private http: Http, private apiService: ApiService) {
    this.api_url = apiService.getUrl() + '/staff';
    this.headers = apiService.getHeaders();
  }
  getUser(): Promise<User[]> {
    const url = `${this.api_url}/all`;
    return this.http.get(url, {headers: this.headers})
      .toPromise()
      .then(res => res.json() as User[])
      .catch(this.handleError);
  }

  getUserById(id: number): Promise<User[]> {
    const url = `${this.api_url}/one/${id}`;
    return this.http.get(url, {headers: this.headers})
      .toPromise()
      .then(res => res.json() as User[])
      .catch(this.handleError);
  }

  selectUser(uname: string, pw: string): Promise<User[]> {
    const url = `${this.api_url}/select`;
    let user = {
      sid: uname,
      sname: '',
      job: '',
      pass: pw,
      level: ''
    };
    return this.http.post(url, JSON.stringify(user), {headers: this.headers})
      .toPromise()
      .then(res => res.json() as User[])
      .catch(this.handleError);
  }

  createUser(user: User): Promise<User> {
    const url = `${this.api_url}`;
    return this.http
      .post(url, JSON.stringify(user), {headers: this.headers})
      .toPromise()
      .then(res => res.json() as User)
      .catch(this.handleError);
  }

  createConggeUser(type: string, st: string, et: string, reason: string): Promise<User> {
    const url = `${this.api_url}/insert`;
    let user = {
      lid: 0,
      sid: 4,
      ltype: type,
      startTime: st,
      endTime: et,
      reason: reason,
      state: '未通过'
    };
    let s = JSON.stringify(user);
    return this.http
      .post(url, s, {headers: this.headers})
      .toPromise()
      .then(res => res.json() as User)
      .catch(this.handleError);
  }

  updateUser(id: number, user: string): Promise<User> {
    const url = `${this.api_url}/update/${id}`;
    return this.http
      .post(url, user, { headers: this.headers })
      .toPromise()
      .then(res => res.json() as User)
      .catch(this.handleError);
  }

  deleteUser(id: number): Promise<void> {
    const url = `${this.api_url}/delete`;
    let user = {
      lid: id,
      sid: '',
      ltype: '',
      startTime: '',
      endTime: '',
      reason: '',
      state: ''
    };
    return this.http
      .post(url, JSON.stringify(user) , { headers: this.headers })
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }

}
