import { Headers } from '@angular/http';

export class ApiService {
  getUrl(): string {
    return 'http://172.26.168.95:58849/api';
    // return 'http://localhost:3000';
  }
  getHeaders(): Headers {
    return new Headers({'Content-Type': 'application/json'});
  }
}
