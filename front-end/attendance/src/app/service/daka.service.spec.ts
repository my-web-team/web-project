import { TestBed } from '@angular/core/testing';

import { DakaService } from './daka.service';

describe('DakaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DakaService = TestBed.get(DakaService);
    expect(service).toBeTruthy();
  });
});
