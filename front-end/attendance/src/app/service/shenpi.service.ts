import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/operators';
import { Shenpi } from '../domain/shenpi';
import { ApiService } from './api.service';
import { ShenpiComponent } from '../shenpi/shenpi.component';

@Injectable()
export class ShenpiService {
  private api_url ;
  private headers ;

  constructor(private http: Http, private apiService: ApiService) {
    this.api_url = apiService.getUrl() + '/approval';
    this.headers = apiService.getHeaders();
  }
  getShenpi(): Promise<Shenpi[]> {
    const url = `${this.api_url}/all`;
    return this.http.get(url, {headers: this.headers})
      .toPromise()
      .then(res => res.json() as Shenpi[])
      .catch(this.handleError);
  }

  getShenpiById(id: number): Promise<Shenpi[]> {
    const url = `${this.api_url}/one/${id}`;
    return this.http.get(url, {headers: this.headers})
      .toPromise()
      .then(res => res.json() as Shenpi[])
      .catch(this.handleError);
  }

  createConggeShenpi(cTime: string): Promise<Shenpi> {
    const url = `${this.api_url}/insert`;
    let shenpi = {
      cid: 0,
      sid: 4,
      cTime: cTime
    };
    let s = JSON.stringify(shenpi);
    return this.http
      .post(url, s, {headers: this.headers})
      .toPromise()
      .then(res => res.json() as Shenpi)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }

}
