import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/operators';

import { Qingjia } from '../domain/qingjia';

import { ApiService } from './api.service';
import {Daka} from '../domain/daka';

@Injectable()
export class QingjiaService {
  private api_url ;
  private headers ;

  constructor(private http: Http, private apiService: ApiService) {
    this.api_url = apiService.getUrl() + '/leaves';
    this.headers = apiService.getHeaders();
  }
  getQingjia(): Promise<Qingjia[]> {
    const url = `${this.api_url}/all`;
    return this.http.get(url, {headers: this.headers})
      .toPromise()
      .then(res => res.json() as Qingjia[])
      .catch(this.handleError);
  }

  getQingjiaById(id: number): Promise<Qingjia[]> {
    const url = `${this.api_url}/one/${id}`;
    return this.http.get(url, {headers: this.headers})
      .toPromise()
      .then(res => res.json() as Qingjia[])
      .catch(this.handleError);
  }

  selectQingjia(lid: number, sid: string, ltype: string, startTime: string,
             endTime: string, reason: string, state: string): Promise<Qingjia[]> {
    const url = `${this.api_url}/select`;
    let qingjia = {
      lid: lid,
      sid: sid,
      ltype: ltype,
      startTime: startTime,
      endTime: endTime,
      reason: reason,
      state: state,
    };
    return this.http.post(url, JSON.stringify(qingjia), {headers: this.headers})
      .toPromise()
      .then(res => res.json() as Qingjia[])
      .catch(this.handleError);
  }

  createQingjia(qingjia: Qingjia): Promise<Qingjia> {
    const url = `${this.api_url}`;
    return this.http
      .post(url, JSON.stringify(qingjia), {headers: this.headers})
      .toPromise()
      .then(res => res.json() as Qingjia)
      .catch(this.handleError);
  }

  createConggeQingjia(sid: string, type: string, st: string, et: string, reason: string): Promise<Qingjia> {
    const url = `${this.api_url}/insert`;
    let qingjia = {
      lid: 0,
      sid: sid,
      ltype: type,
      startTime: st,
      endTime: et,
      reason: reason,
      state: '待定'
    };
    let s = JSON.stringify(qingjia);
    return this.http
      .post(url, s, {headers: this.headers})
      .toPromise()
      .then(res => res.json() as Qingjia)
      .catch(this.handleError);
  }

  updateQingjia(id: number, qingjia: string): Promise<Qingjia> {
    const url = `${this.api_url}/update/${id}`;
    return this.http
      .post(url, qingjia, { headers: this.headers })
      .toPromise()
      .then(res => res.json() as Qingjia)
      .catch(this.handleError);
  }

  deleteQingjia(id: number): Promise<void> {
    const url = `${this.api_url}/delete`;
    let qingjia = {
      lid: id,
      sid: '',
      ltype: '',
      startTime: '',
      endTime: '',
      reason: '',
      state: ''
    };
    return this.http
      .post(url, JSON.stringify(qingjia) , { headers: this.headers })
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }

}
