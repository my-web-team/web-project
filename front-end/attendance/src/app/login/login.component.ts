import { Component, OnInit } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { UserComponent } from '../user/user.component';
import { UserService } from '../service/user.service';
import { User } from '../domain/user';
import { promise } from 'protractor';
import { template } from '@angular/core/src/render3';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private userService: UserService, private router: Router) {}

  title = 'attendance';
  user: User [];

  ngOnInit() {
  }

  check() {
    localStorage.removeItem('userId');
    localStorage.removeItem('level');
    this.userService.selectUser(
      document.getElementById('userid').value,
      document.getElementById('password').value)
      .then(user => this.user = user);
    console.log('user', (document.getElementById('userid') as HTMLInputElement).value);
    console.log('psw', (document.getElementById('password') as HTMLInputElement).value);
    if (this.user[0] == null) {
      return false;
    }
    localStorage.setItem('userId', (this.user[0] as User).sid);
    localStorage.setItem('level', (this.user[0] as User).level);
    console.log('id', localStorage.getItem('userId'));
    console.log('level', localStorage.getItem('level'));
    console.log('full', this.user[0]);
    if (localStorage.getItem('userId') != null) {
      return true;
    } else {
      return false;
    }
  }
  submit_click() {
    if(this.check()===true)
    {
     var  tmp = document.getElementById('dl');
     tmp.style.display = 'none';
     tmp = document.getElementById('denglu');
     tmp.style.display = 'none';
     tmp=document.getElementById('zhuce');
     tmp.style.display='none';

     tmp=document.getElementById('zhongxin');
     tmp.style.display='inline';
     tmp=document.getElementById('zhuxiao');
     tmp.style.display='inline';
     this.router.navigate([localStorage.getItem('redirectUrl')]);
    }
    else
    {
      alert("用户名或密码错误!");
    }
    this.user = [];
  }
  cancel_click()
  {
    this.router.navigate(['']);
    
  }
}
