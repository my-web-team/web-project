import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QingjiaComponent } from './qingjia.component';

describe('QingjiaComponent', () => {
  let component: QingjiaComponent;
  let fixture: ComponentFixture<QingjiaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QingjiaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QingjiaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
