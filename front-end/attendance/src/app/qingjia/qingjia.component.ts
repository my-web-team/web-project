import { Component, OnInit } from '@angular/core';
import { QingjiaService } from '../service/qingjia.service';
import { Qingjia } from '../domain/qingjia';

@Component({
  selector: 'app-qingjia',
  templateUrl: './qingjia.component.html',
  styleUrls: ['./qingjia.component.css']
})
export class QingjiaComponent implements OnInit {

  constructor(private qingjiaService: QingjiaService) { }

  qingjias: Qingjia [];
  allQingjia: Qingjia [];
  selectedQingjia: Qingjia [];
  rId: number;

  ngOnInit() {
    this.qingjiaService.selectQingjia(0, localStorage.getItem('userId'), '', '', '', '', '')
      .then(qingjias => this.qingjias = qingjias);
    this.qingjiaService.getQingjia()
      .then(qingjias => this.allQingjia = qingjias);
  }

  addQingjia(type: string, st: string, et: string, reason: string) {
    st = st.trim();
    et = et.trim();
    reason = reason.trim();
    var r = confirm('确认提交吗？');
    if (r == true) {
      if (!type || !st || !et || !reason) {
        alert('请填写完全你的理由！');
        return false;
      } else {
        this.qingjiaService.createConggeQingjia(localStorage.getItem('userId'), type, st, et, reason);
        alert('你已经提交了请求！');
        return true;
      }
    } else {
      alert('已取消操作');
      return false;
    }
  }
  deleteQingjia(id: number) {
    this.qingjiaService.deleteQingjia(id);
  }

  updateQingjia(id: number) {
    this.rId = id;
    this.qingjiaService.selectQingjia(id, '', '', '', '', '', '')
      .then(qingjias => this.selectedQingjia = qingjias);
    if ((this.selectedQingjia[0] as Qingjia).state === '待定') {
      document.getElementById('renew').style.display = 'block';
      (document.getElementById('r-types') as HTMLInputElement).value = (this.selectedQingjia[0] as Qingjia).ltype;
      (document.getElementById('r-st') as HTMLInputElement).value = (this.selectedQingjia[0] as Qingjia).startTime;
      (document.getElementById('r-et') as HTMLInputElement).value = (this.selectedQingjia[0] as Qingjia).endTime;
      (document.getElementById('r-reason') as HTMLInputElement).value = (this.selectedQingjia[0] as Qingjia).reason;
    } else {
      alert('本状态无法更改！');
    }
  }

  rAddQingjia(type: string, st: string, et: string, reason: string) {
    let qingjia = {
      lid: this.rId,
      sid: localStorage.getItem('userId'),
      ltype: type,
      startTiime: st,
      endTime: et,
      reason: reason,
      state: '待定'
    };
    this.qingjiaService.updateQingjia(this.rId, JSON.stringify(qingjia));
    document.getElementById('renew').style.display = 'none';
  }
}
