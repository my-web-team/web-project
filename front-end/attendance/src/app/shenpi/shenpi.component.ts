import { Component, OnInit } from '@angular/core';
import { Shenpi } from '../domain/shenpi';
import { Qingjia } from '../domain/qingjia';
import { Waichu } from '../domain/waichu';
import { ShenpiService } from '../service/shenpi.service';
import { QingjiaService } from '../service/qingjia.service';
import { ShenpiService } from '../service/shenpi.service';
import { WaichuService } from '../service/waichu.service';

@Component({
  selector: 'app-shenpi',
  templateUrl: './shenpi.component.html',
  styleUrls: ['./shenpi.component.css']
})
export class ShenpiComponent implements OnInit {

  constructor(private shenpiService: ShenpiService,
              private qingjiaService: QingjiaService,
              private waichuService: WaichuService) { }

  shenpis: Shenpi [];
  qingjias: Qingjia [];
  waichus: Waichu [];
  selectedshenpi: Shenpi [];
  selectedWaichu: Waichu [];
  selectedQingjia: Qingjia [];
  rId: number;
  agree: string;

  ngOnInit() {
    this.qingjiaService.selectQingjia(0,'','','','','','待定')
      .then(qingjias => this.qingjias = qingjias);
    this.waichuService.selectWaichu(0,'','','','','待定')
      .then(waichus => this.waichus = waichus);
  }
  updateWaichu(id: number) {
    this.rId = id;
    this.waichuService.selectWaichu(id, '', '', '', '', '')
      .then(waichus => this.selectedWaichu = waichus);
    document.getElementById('Wshenpi').style.display = 'block';
  }
  updateQingjia(id: number) {
    this.rId = id;
    this.qingjiaService.selectQingjia(id, '', '', '', '', '', '')
      .then(qingjias => this.selectedQingjia = qingjias);
    document.getElementById('Qshenpi').style.display = 'block';
  }
  agree() {
    this.agree = '通过';
  }
  disagree() {
    this.agree = '拒绝';
  }
  Wsubmit(suggest: string) {
    let waichu = {
      oid: 0,
      sid: '',
      startTime: '',
      endTime: '',
      reason: '',
      state: this.agree
    };
    this.waichuService.updateWaichu(this.rId, JSON.stringify(waichu));
    document.getElementById('Wrenew').style.display = 'none';
  }
  Qsubmit(suggest: string) {
    let qingjia = {
      lid: 0,
      sid: '',
      ltype: '',
      startTime: '',
      endTime: '',
      reason: '',
      state: this.agree
    };
    this.waichuService.updateWaichu(this.rId, JSON.stringify(qingjia));
    document.getElementById('Qrenew').style.display = 'none';
  }
}
