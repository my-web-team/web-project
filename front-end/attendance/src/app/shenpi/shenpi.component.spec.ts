import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShenpiComponent } from './shenpi.component';

describe('ShenpiComponent', () => {
  let component: ShenpiComponent;
  let fixture: ComponentFixture<ShenpiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShenpiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShenpiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
