use company;
DROP TABLE staff;
DROP TABLE approval;
DROP TABLE clockRecord;
DROP TABLE outRecord;
DROP TABLE leaveRecord;
Create table staff(
sid  int PRIMARY KEY not null AUTO_INCREMENT,
sname  char(10),
job   char(10),
pass char(20),
level int
);
Create table approval(
aid int PRIMARY KEY not null AUTO_INCREMENT,
atype enum('l','o'),
tid int,
suggest  char(10),
ok  bool,
aTime datetime
);
Create table clockRecord(
cid int PRIMARY KEY not null AUTO_INCREMENT,
sid int,
ctime datetime
);
Create table outRecord(
oid int PRIMARY KEY not null AUTO_INCREMENT,
sid  int,
startTime date,
endTime datetime,
reason  char(100),
state char(10) DEFAULT "待定"
);
Create table leaveRecord(
lid int PRIMARY KEY not null AUTO_INCREMENT,
sid int,
ltype  char(10),
startTime datetime,
endTime datetime,
reason  char(100),
state char(10) DEFAULT "待定"
);
insert into staff VALUES("001","陈独秀","普通员工","123456",1);
insert into staff VALUES("002","包拯","行政部员工","123456",2);
insert into staff VALUES("003","李大钊","普通员工","654321",1);
insert into staff VALUES("004","李子明","普通员工","13579",2);
insert into staff VALUES("005","李罡","总经理","123456",2);
insert into leaveRecord VALUES("001","001","病假",'1942-05-27 06:00:00','2018-12-14 00:00:00',"沉眠","待定");
insert into outRecord VALUES("001","001",'1942/05/27','2018-12-14',"To the heaven","待定");